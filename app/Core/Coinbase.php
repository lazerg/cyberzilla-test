<?php

namespace App\Core;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class Coinbase
{
    /**
     * Get a oauth url
     *
     * @param User $user
     * @return string
     */
    public function getOAuthLink(User $user): string
    {
        $params = collect([
            'response_type' => 'code',
            'client_id'     => config('coinbase.client_id'),
            'scope'         => 'wallet:accounts:read,wallet:transactions:request',
            'redirect_uri'  => url('coinbase-response', $user),
        ])
            ->map(fn($value, $key) => implode('=', [$key, $value]))->join('&');

        return config('coinbase.url_oauth') . "?" . $params;
    }

    public function requestMoney(User $user, string $temporaryCode): bool
    {
        $token = $this->exchangeCodeToToken($temporaryCode);
        $userData = $this->getUserByToken($token);
        $this->requestPayment($user, $token, $userData['id']);

        return true;
    }

    /**
     * Exchange code to token
     *
     * @param string $code
     *
     * @return string
     */
    private function exchangeCodeToToken(string $code): string
    {
        return Http::post(config('coinbase.url_token'), [
            'grant_type'    => 'authorization_code',
            'code'          => $code,
            'client_id'     => config('coinbase.client_id'),
            'client_secret' => config('coinbase.client_secret'),
            'redirect_uri'  => url()->current(),
        ])
            ->json('access_token');
    }

    /**
     * Get user data by token
     *
     * @param string $token
     * @return array
     */
    private function getUserByToken(string $token): array
    {
        return Http::withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->get(config('coinbase.url_user'))
            ->json('data');
    }

    /**
     * Request test payment
     *
     * @param User $user
     * @param string $token
     * @param string $accountId
     * @return array
     */
    private function requestPayment(User $user, string $token, string $accountId): array
    {
        $data = [
            'currency' => 'BTC',
            'amount'   => '0.0001',
            'to' => 'lazerg2@gmail.com'
        ];

        $user->payments()->create($data);

        return Http::withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->post("https://api.coinbase.com/v2/accounts/$accountId/transactions", [
                'type'     => 'request',
            ] + $data)
            ->json();
    }


    /**
     * Request payment from temporary code
     *
     * @param string $code
     */
    public function sendPayment(string $temporaryCode)
    {
        $access_token = $this->exchangeCodeToToken($temporaryCode);
        $this->sendMoney($access_token, 'lazerg2@gmail.com');
    }


    private function sendMoney(string $token, string $to, string $amount = '0.1', $currency = 'BTC')
    {
        $id = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->get(config('coinbase.url_user'))->json('data.id');

        return Http::withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])
            ->post("https://api.coinbase.com/v2/accounts/$id/transactions", [
                'type'     => 'request',
                'to'       => $to,
                'amount'   => $amount,
                'currency' => $currency,
            ])
            ->json();
    }
}
