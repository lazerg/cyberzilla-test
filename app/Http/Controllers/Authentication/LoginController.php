<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    public function __invoke(Request $request)
    {
        $data = $request->validate([
            'email'    => 'required|email|exists:users',
            'password' => 'required'
        ]);

        if (!Auth::attempt($data, true)) {
            return response()->noContent(Response::HTTP_FORBIDDEN);
        }

        return $this->responseData([
            'token' => auth()->user()->createToken('authentication')->plainTextToken
        ]);
    }
}
