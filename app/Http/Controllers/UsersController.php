<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    /**
     * Index all users
     *
     * @return JsonResponse
     */
    public function index()
    {
        $users = User::latest()->get();

        return $this->responseResourceful(UserResource::class, $users);
    }

    /**
     * Show a user
     *
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse
     */
    public function show(Request $request, User $user): JsonResponse
    {
        return $this->responseResourceful(UserResource::class, $user);
    }

    /**
     * Store a new user
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->validate([
            'name'     => 'required|min:6',
            'tel'      => 'required|min:7',
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        $data['password'] = Hash::make($data['password']);

        $user = User::create($request->all());

        return $this->response('User has been successfully created', $user, Response::HTTP_CREATED);
    }

    /**
     * Update a user
     *
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse
     */
    public function update(Request $request, User $user): JsonResponse
    {
        $data = $request->validate([
            'name'     => 'min:6',
            'tel'      => 'min:7',
            'email'    => 'email|unique:users',
            'password' => 'min:6',
        ]);

        $user->update($data);

        return $this->response('User has been successfully updated', $user);
    }

    /**
     * Destroy a user
     *
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse
     */
    public function destroy(Request $request, User $user): JsonResponse
    {
        $user->delete();

        return $this->response('User has been successfully deleted', $user);
    }
}
