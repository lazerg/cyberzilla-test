<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CoinbaseController extends Controller
{
    /**
     * Handle code returned from coinbase
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(Request $request, User $user): \Illuminate\Http\RedirectResponse
    {
        app('coinbase')
            ->requestMoney($user, $request->get('code'));

        return Redirect::to(route('users-page'));
    }
}
