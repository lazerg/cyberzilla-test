<?php

namespace App\Http\Controllers;

use App\Http\Resources\PaymentResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    /**
     * Return all payments of $user
     *
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse
     */
    public function index(Request $request, User $user): JsonResponse
    {
        return $this->responseResourceful(PaymentResource::class, $user->payments);
    }
}
