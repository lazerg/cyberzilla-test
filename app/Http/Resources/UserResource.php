<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\User */
class UserResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'email'       => $this->email,
            'tel'         => $this->tel,
            'o_auth_link' => $this->o_auth_link,
            'created_at'  => $this->created_at,
        ];
    }
}
