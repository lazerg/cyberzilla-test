<?php

namespace App\Http\Resources;

use App\Models\Payment;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Payment */
class PaymentResource extends JsonResource
{
    /**
     * Resource
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'       => $this->id,
            'date'     => $this->created_at->toDateTimeString(),
            'amount'   => $this->amount,
            'to'       => $this->to,
            'currency' => $this->currency,
        ];
    }
}
