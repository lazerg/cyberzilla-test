<?php

namespace App\Providers;

use App\Core\Coinbase;
use Illuminate\Support\ServiceProvider;

class CoinbaseProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('coinbase', function () {
            return new Coinbase();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
