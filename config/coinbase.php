<?php

return [
    'url_oauth'     => 'https://www.coinbase.com/oauth/authorize',
    'url_token'     => 'https://www.coinbase.com/oauth/token',
    'url_user'      => 'https://api.coinbase.com/v2/user',
    'client_id'     => '797272c30cc5dd8542df3bac19494bf1df179b300bf400f2b5a7c9ac6f2d7968',
    'client_secret' => 'e4b5864ccf9a6f3cac3f86cebcae84671d3616e661f4766bab5c154236e8e6a7',
];
