<?php

namespace Tests;

use App\Models\Payment;
use App\Models\User;
use Illuminate\Support\Collection;

trait FactoryHelpers
{
    /**
     * @param int|null $count
     * @return User|Collection
     */
    protected function createUser(?int $count = null): User|Collection
    {
        return User::factory($count)->create();
    }

    /**
     * @param int|null $count
     * @return Payment|Collection
     */
    protected function createPayment(?int $count = null): Payment|Collection
    {
        return Payment::factory($count)->create();
    }
}
