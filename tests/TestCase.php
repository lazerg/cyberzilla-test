<?php

namespace Tests;

use Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations, FactoryHelpers;

    protected function setUp(): void
    {
        parent::setUp();

        $this->withExceptionHandling();

        Artisan::call('db:seed');
    }
}
