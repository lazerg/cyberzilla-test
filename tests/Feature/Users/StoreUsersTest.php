<?php

namespace Tests\Feature\Users;

use Arr;
use Tests\TestCase;

class StoreUsersTest extends TestCase
{
    public function test_creating_a_new_user()
    {
        $data = [
            'name'     => 'Mr Lazizbek',
            'tel'      => '+998936268547',
            'email'    => 'mrlazizbek@gmail.com',
            'password' => '123123'
        ];

        $dataWithoutPassword = Arr::except($data, 'password');

        $this
            ->postJson(route('users.store'), $data)
            ->assertCreated()
            ->assertJsonData($dataWithoutPassword);

        $this->assertDatabaseHas('users', $dataWithoutPassword);
    }
}
