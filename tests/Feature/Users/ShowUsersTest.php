<?php

namespace Tests\Feature\Users;

use Tests\TestCase;

class ShowUsersTest extends TestCase
{
    public function test_show_a_user()
    {
        $this->getJson(route('users.show', 1))
            ->assertOk()
            ->assertJsonData([
                'id'   => 1,
                'name' => 'Lazizbek',
            ]);
    }
}
