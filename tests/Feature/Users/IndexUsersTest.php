<?php

namespace Tests\Feature\Users;

use Tests\TestCase;

class IndexUsersTest extends TestCase
{
    public function test_get_all_users_list()
    {
        $this->getJson(route('users.index'))
            ->assertOk()
            ->assertJsonCountData(1)
            ->assertJsonData([[
                'id'   => 1,
                'name' => 'Lazizbek',
            ]]);
    }
}
