<?php

namespace Tests\Feature\Users;

use Tests\TestCase;

class UpdateUsersTest extends TestCase
{
    public function test_update_a_user_data()
    {
        $changes = [
            'email' => 'johndoe@gmail.com',
            'name'  => 'John Doe',
        ];

        $this->patchJson(route('users.update', 1), $changes)
            ->assertOk()
            ->assertJsonData($changes);

        $this->assertDatabaseHas('users', $changes);
    }
}
