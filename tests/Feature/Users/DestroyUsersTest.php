<?php

namespace Tests\Feature\Users;

use Tests\TestCase;

class DestroyUsersTest extends TestCase
{
    public function test_destroying_users()
    {
        $this->assertDatabaseHas('users', ['id' => 1]);

        $this->deleteJson(route('users.destroy', 1))->assertOk();

        $this->assertDatabaseMissing('users', ['id' => 1]);
    }
}
