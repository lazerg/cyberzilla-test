<?php

namespace Tests\Feature\Authentication;

use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function test_authenticating_user()
    {
        $this->postJson(route('auth.login'), [
            'email'    => 'lazerg2@gmail.com',
            'password' => 'password',
        ])
            ->assertOk()
            ->assertJsonStructureData(['token']);
    }
}
