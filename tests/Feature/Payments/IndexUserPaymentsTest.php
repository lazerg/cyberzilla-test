<?php

namespace Tests\Feature\Payments;

use App\Models\User;
use Tests\TestCase;

class IndexUserPaymentsTest extends TestCase
{
    public function test_index_user_payments()
    {
        $this_user = User::find(1);
        $another_user = $this->createUser();

        $this_user->payments()->saveMany(
            $this->createPayment(15)
        );

        $another_user->payments()->saveMany(
            $this->createPayment(15)
        );

        $response = $this->getJson(route('users.payments.index', $this_user))
            ->assertOk()
            ->assertJsonCountData(15);
    }
}
