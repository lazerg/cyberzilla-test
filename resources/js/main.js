import Vue from "vue";
import { createInertiaApp, Link } from "@inertiajs/inertia-vue";
import "./plugins/axios";
import "./plugins/devextreme";

// noinspection JSIgnoredPromiseFromCall
createInertiaApp({
    resolve: name => require(`./views/${ name }.vue`).default,
    setup({ el, App, props }) {
        new Vue({
            render: h => h(App, props),
        }).$mount(el)
    },
})
