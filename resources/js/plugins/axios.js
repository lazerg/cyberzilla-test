import axios from 'axios';
import notify from 'devextreme/ui/notify';


const instance = axios.create();

const handleSuccessResponse = response => {
    if (response.data.message) {
        notify(response.data.message, 'success', 2000);
    }

    return response.data.data;
}

const handleErrorResponse = error => {
    if (error.response.data.message) {
        notify(error.response.data.message, 'error', 2000);
    }

    return error;
};

instance.interceptors.response.use(handleSuccessResponse, handleErrorResponse);

window.axios = instance;
